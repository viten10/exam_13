import galleriesSlice from "../slices/galleriesSlice";

export const {
    fetchGalleryRequest,
    fetchGallerySuccess,
    fetchGalleryFailure,
    sendImageInstitutionByUserRequest,
    sendImageInstitutionByUserSuccess,
    sendImageInstitutionByUserFailure,

} = galleriesSlice.actions;