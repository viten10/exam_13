import React, {useEffect, useState} from 'react';
import {clearErrorUser, loginUserRequest} from "../../store/actions/usersActions";
import {Link, Link as RouterLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {Alert} from "@mui/material";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {newUserRegister} from "../../paths";
import {makeStyles} from "@mui/styles";


const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.success.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%",
    },
}));

const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.user.loginError);
    const loginIn = useSelector(state => state.user.loginLoading);
    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    useEffect(() => {
        return () => {
            dispatch(clearErrorUser());
        };
    }, [dispatch]);

    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUserRequest({...user}));
    };
    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    {/*<LockOpenOutlinedIcon/>*/}
                </Avatar>
                <Typography component="h1" variant="h6">
                    Sign in
                </Typography>
                {
                    error &&
                    <Alert severity="error" className={classes.alert}>
                        {error.message || error.global}
                    </Alert>
                }
                <Grid
                    component="form"
                    container
                    className={classes.form}
                    onSubmit={submitFormHandler}
                    spacing={2}
                >
                    <FormElement
                        type="text"
                        autoComlete="current-email"
                        label="Email"
                        name="email"
                        value={user.email}
                        onChange={inputChangeHandler}
                    />

                    <FormElement
                        type="password"
                        autoComlete="current-username"
                        label="Password"
                        name="password"
                        value={user.password}
                        onChange={inputChangeHandler}
                    />

                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            loading={loginIn}
                            disabled={loginIn}
                        >
                            Sign in
                        </ButtonWithProgress>
                    </Grid>

                    <Grid item container justifyContent="flex-end">
                        <Link component={RouterLink} variant="body2" to={newUserRegister}>
                            Or Sign up
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Login;