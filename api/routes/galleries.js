const express = require("express");
const auth = require("../middleware/auth");
const multer = require("multer");
const config = require("../config");
const permit = require("../middleware/permit");
const Gallery = require('../models/Gallery');


const storage = multer.diskStorage({
    destination: async (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: async (req, file, cb) => {
            cb(null, file.originalname);
    }
});

const upload = multer({storage});


const router = express.Router();

router.get ('/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const response = await Gallery.find({institution:id});
        res.send(response);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post ('/', auth, permit('user'), upload.single('image'), async (req, res) => {
    const data = {};
    try {

        if (req.body.user) data.user = req.body.user;
        if (req.body.institution) data.institution = req.body.institution;
        if (req.file) {
            data.image = 'uploads/' + req.file.filename;
        }
        const gallery = new Gallery(data);
        await gallery.save();

        res.send(gallery);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.put ('/', auth, async (req, res) => {
    try {

    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete ('/', auth, async (req, res) => {
    try {

    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;