const mongoose = require('mongoose');

const InstitutionSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
        unique: true,
        required: true,
    },
    description: {
        type: String,
        trim: true,
    },
    review: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Review',
    }],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    gallery: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Gallery',
    }],
    mainImage: {
        type: String,
        trim: true,
    },
    totalRating: {
        kitchen: Number,
        service: Number,
        atmosphere: Number,
    },
    reviewCount: {
        type: Number,
        default: 0,
    },
});

const Institution = mongoose.model('Institution', InstitutionSchema);

module.exports = Institution;