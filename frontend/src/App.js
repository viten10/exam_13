import React from 'react';
import {Route, Routes} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import {institution, mainPage, newStore, newUserRegister, userLogin,} from "./paths";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import Root from "./containers/Root/Root";
import InstitutionSingle from "./components/InstitutionSingle/InstitutionSingle";
import CreateStore from "./containers/CreateStore/CreateStore";

const App = () => {
    return (
        <Layout>
            <Routes>
                <Route path={mainPage} element={<Root/>}/>
                <Route path={newUserRegister} element={<Register/>}/>
                <Route path={userLogin} element={<Login/>}/>
                <Route path={institution} element={<InstitutionSingle/>}/>
                <Route path={newStore} element={<CreateStore/>}/>

                <Route
                    path="*"
                    element={
                        <main style={{padding: "1rem"}}>
                            <h1>Page Not Found</h1>
                        </main>
                    }
                />
            </Routes>
        </Layout>
    );
};

export default App;