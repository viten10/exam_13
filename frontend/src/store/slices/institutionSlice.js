import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    data: [],
    loading: false,
    errorLoad: null,
};

const name = 'institutions';

const institutionSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchInstitutionRequest(state) {
            state.loading = true;
        },
        fetchInstitutionSuccess(state, {payload: data}) {
            state.loading = false;
                state.data = data;
        },
        fetchInstitutionFailure(state,{payload: data}) {
            state.loading = false;
            state.errorLoad = data;
        },
        createInstitutionRequest(state) {
            state.loading = true;
        },
        createInstitutionSuccess(state) {
            state.loading = false;
        },
        createInstitutionFailure(state,{payload: data}) {
            state.loading = false;
            state.errorLoad = data;
        },
        increaseCountRequest(state) {
            state.loading = true;
        },
        increaseCountSuccess(state) {
            state.loading = false;
        },
        increaseCountFailure(state,{payload: data}) {
            state.loading = false;
            state.errorLoad = data;
        },
    }
});

export default institutionSlice;