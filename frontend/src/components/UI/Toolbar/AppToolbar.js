import React from 'react';
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {AppBar, Box, LinearProgress, Toolbar} from "@mui/material";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import UserMenu from "./Menu/UserMenu";
import Anonymous from "./Menu/Anonymous";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles(theme => ({
    mainLink: {
        color: "inherit",
        textDecoration: 'none',
        '$:hover': {
            color: 'inherit'
        }
    },
    staticToolbar: {
        marginBottom: theme.spacing(2)
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    progress: {
        position: "absolute",
        left: 0,
        bottom: 0,
        zIndex: theme.zIndex.drawer + 999,
    }
}));

const AppToolbar = () => {
    const classes = useStyles();
    const user = useSelector(state => state.user.info);
    const loading = useSelector(state => state.user.loading);

    return (
        <>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar position='relative'>
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Typography variant="h6">
                            <Link to="/" className={classes.mainLink}>CHANGE NAME</Link>
                        </Typography>
                        <Grid>
                            {
                                user ? <UserMenu user={user}/> : <Anonymous/>
                            }
                        </Grid>
                    </Grid>
                </Toolbar>
                {loading && <LinearProgress className={classes.progress} />}
            </AppBar>
            <Toolbar className={classes.staticToolbar}/>

        </>
    );
};

export default AppToolbar;