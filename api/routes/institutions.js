const express = require("express");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Institution = require('../models/Institution');
const multer = require("multer");
const config = require("../config");

const storage = multer.diskStorage({
    destination: async (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: async (req, file, cb) => {
        cb(null, file.originalname);
    }
});

const upload = multer({storage});

const router = express.Router();

router.get ('/', async (req, res) => {
    try {
        const responseInstitutions = await Institution.find();

        res.send(responseInstitutions);
   } catch (e) {
       res.status(500).send(e);
   }
});

router.post ('/', auth, permit('user', 'admin'), upload.single('mainImage'), async (req, res) => {
    const data = {}
    try {
        if (!req.body.permit) return res.status(400).send('Не верно введены данные')
        if (req.body.user) data.user = req.body.user;
        if (req.body.title) data.title = req.body.title;
        if (req.body.description) data.description = req.body.description;
        if (req.file) {
            data.mainImage = 'uploads/' + req.file.filename;
        }
        const  institution = new Institution(data);
        await institution.save();


        res.send(institution);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.put ('/:id', async (req, res) => {
    const id = req.params.id;
    try {
        await Institution.findByIdAndUpdate(id, {$inc:{reviewCount: 1}})
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete ('/', auth, async (req, res) => {
    console.log('in delete Institutions');

    try {

    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;