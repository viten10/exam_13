import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import institutionSagas from "./sagas/institutionSagas";
import galleriesSagas from "./sagas/galleriesSagas";
import reviewsSagas from "./sagas/reviewsSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...institutionSagas,
        ...galleriesSagas,
        ...reviewsSagas,
    ]);
}