const express = require("express");
const User = require('../models/User');
const config = require('../config');
const {nanoid} = require("nanoid");
const multer = require("multer");
const path = require("path");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.post('/', upload.single('avatar'), async (req, res) => {
    if (!req.body.email || !req.body.password || !req.body.name) {
        return res.status(400).send({error: 'Data No Valid'});
    }

    const userData = {
        email: req.body.email,
        password: req.body.password,
        name: req.body.name,
    }

    if (req.file) {
        userData.avatar = req.file.filename;
    }

    const user = new User(userData);

    try {
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/sessions', async (req, res) => {
    try {
        const user = await User.findOne({email: req.body.email});

        if (!user) {
            return res.status(401).send({message: "Something went wrong"});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(401).send({message: "Something went wrong"});
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(user);
    } catch (e) {
        res.status(500).send(e);
    }

});

router.delete('/sessions', async (req, res) => {
   try {
       const token = req.get('Authorization');
       const success = {message: 'Success'};
       const user = await User.findOne({token});
       user.generateToken();
       await user.save({validateBeforeSave: false});
       res.send(success);
   } catch (e) {
       res.status(500).send(e);
   }
});

module.exports = router;