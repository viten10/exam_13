import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
    createInstitutionFailure,
    createInstitutionRequest, createInstitutionSuccess,
    fetchInstitutionFailure,
    fetchInstitutionRequest,
    fetchInstitutionSuccess, increaseCountFailure, increaseCountRequest, increaseCountSuccess
} from "../actions/institutionActions";
import History from "../../History";
import {mainPage} from "../../paths";



export function* requestDataInstitution() {
    try {
        const response = yield axiosApi.get('/institution');
        yield put(fetchInstitutionSuccess(response.data));
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(fetchInstitutionFailure(e.response.data));
    }
}

export function* createDataInstitution({payload}) {
    try {
        yield axiosApi.post('/institution', payload);
        yield put(createInstitutionSuccess());
        History.push(mainPage);
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(createInstitutionFailure(e.response.data));
    }
}

export function* increaseCountInstitution({payload}) {
    console.log('ok');
    try {
        yield axiosApi.put(`/institution/${payload.id}`, {increase: 1});
    } catch (e) {
    }
}

const institutionSagas = [
    takeEvery(fetchInstitutionRequest, requestDataInstitution),
    takeEvery(createInstitutionRequest, createDataInstitution),
    takeEvery(increaseCountRequest, increaseCountInstitution),
];

export default institutionSagas;