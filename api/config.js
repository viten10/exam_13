const path = require('path');

const rootPath = __dirname;

const name = 'exam13';

let dbUrl = 'mongodb://localhost/' + name;
let port = 8000;

module.exports = {
    rootPath,
    port,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: dbUrl,
    },
};

