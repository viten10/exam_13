import React from "react";
import Card from '@mui/material/Card';
import InsertPhotoIcon from '@mui/icons-material/InsertPhoto';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {Rating} from "@mui/material";
import {Link} from "react-router-dom";
import {apiURL} from "../../config";
import {institution} from "../../paths";

const MediaCard = ({data, index}) => {
    const image = apiURL+ '/' + data.mainImage;
    const totalR = {};
    if (!data.totalRating) {
        totalR.kitchen = 0
        totalR.service = 0
        totalR.atmosphere = 0;
    }


    const total = (totalR.kitchen + totalR.service + totalR.atmosphere)/3;
    return (
        <Card sx={{width: 245}}>
            <img
                height="140"
                src={image}
                alt='title'
            />
            <CardContent>
                <Typography gutterBottom variant="h6" component="div">
                    <Link to={institution}
                        state={{props:{data,index}}}
                    >
                        {data.title}
                    </Link>
                </Typography>
                <Rating
                    readOnly
                    name="rating"
                    precision={0.1}
                    value={total}
                />
                <Typography variant="body2" color="text.secondary">
                    ({data.reviewCount ? data.reviewCount : 0}, {data.count ? data.count : 0} Просмотров)
                </Typography>
                <Typography
                    variant="body2"
                    color="text.secondary"
                >
                    <InsertPhotoIcon/> {data.gallery.length} фотографий
                </Typography>
            </CardContent>
        </Card>
    )
};
export default MediaCard;

