import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    data: [],
    loading: false,
    errorLoad: null,
};

const name = 'gallery';

const galleriesSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchGalleryRequest(state) {
            state.loading = true;
        },
        fetchGallerySuccess(state, {payload: data}) {
            state.loading = false;
            state.data = data;
        },
        fetchGalleryFailure(state,{payload: data}) {
            state.loading = false;
            state.errorLoad = data;
        },
        sendImageInstitutionByUserRequest(state) {
            state.loading = true;
        },
        sendImageInstitutionByUserSuccess(state, {payload: data}) {
            state.loading = false;
        },
        sendImageInstitutionByUserFailure(state,{payload: data}) {
            state.loading = false;
            state.errorLoad = data;
        },
    }
});

export default galleriesSlice;