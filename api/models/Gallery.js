const mongoose = require('mongoose');

const GallerySchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    institution: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Institution',
    },
    image: {
        type: String,
        trim: true,
        required: true,
    },
});

const Gallery = mongoose.model('Gallery', GallerySchema);

module.exports = Gallery;