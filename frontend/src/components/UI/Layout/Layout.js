import React from 'react';
import {CssBaseline} from "@mui/material";
import AppToolbar from "../Toolbar/AppToolbar";
import Container from "@mui/material/Container";


const Layout = ({children}) => {
    return (
        <>
            <CssBaseline/>
            <AppToolbar/>
            <main>
                <Container>
                    {children}
                </Container>
            </main>
        </>
    );
};

export default Layout;