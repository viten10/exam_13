import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    data: [],
    user: null,
    loading: false,
    errorLoad: null,
};

const name = 'review';

const reviewsSlice = createSlice({
    name,
    initialState,
    reducers: {

        fetchReviewRequest(state) {
            state.loading = true;
        },
        fetchReviewSuccess(state,{payload: data}) {
            state.loading = false;
            state.user = data;
        },
        fetchReviewFailure(state,{payload: data}) {
            state.loading = false;
            state.errorLoad = data;
        },
        sendReviewInstitutionByUserRequest(state) {
            state.loading = true;
        },
        sendReviewInstitutionByUserSuccess(state, {payload: data}) {
            state.loading = false;
            state.data = state.data.push(data);
        },
        sendReviewInstitutionByUserFailure(state,{payload: data}) {
            state.loading = false;
            state.errorLoad = data;
        },
    }
});

export default reviewsSlice;