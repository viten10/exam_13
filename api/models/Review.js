const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    institution: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Institution',
        required: true,
    },

    description: {
        type: String,
        trim: true,
        required: true,
    },
    createDate: {
        type: Date,
        default: Date.now,
    },
    kitchen: {
        type: Number,
        required: true,
    },
    service: {
        type: Number,
        required: true,
    },
    atmosphere: {
        type: Number,
        required: true,
    },
});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;