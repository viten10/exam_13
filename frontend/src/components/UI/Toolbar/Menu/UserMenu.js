import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import MenuItem from "@mui/material/MenuItem";
import {Button, Menu} from "@mui/material";
import {logoutUser} from "../../../../store/actions/usersActions";
import {apiURL} from "../../../../config";
import {Link} from "react-router-dom";
import {newStore} from "../../../../paths";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();
    const avatar = useSelector(state => state.user.info.avatar);
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
                {avatar ? <img src={apiURL + '/' + avatar} width="50" height="50" alt="avatar"/> : null}
                Hello, {user.name}!
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem>Profile</MenuItem>
                <MenuItem component={Link} to={newStore}>Добавить Заведение</MenuItem>
                <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
            </Menu>
        </>);
};

export default UserMenu;