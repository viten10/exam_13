import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    info: null,
    registerLoading: false,
    registerError: null,
    loginError: null,
    loginLoading: false,
    loading: false,
};

const name = 'users';

const usersSlice = createSlice({
    name,
    initialState,
    reducers: {
        registerUserRequest(state) {
            state.registerLoading = true;
            state.loading = true;
        },
        registerUserSuccess(state, {payload: userData}) {
            state.info = userData;
            state.registerLoading = false;
            state.loading = false;
            state.registerError = null;
        },
        registerUserFailure(state, {payload: error}) {
            state.loading = false;
            state.registerLoading = false;
            state.registerError = error;
        },
        loginUserRequest(state) {
            state.loading = true;
            state.loginLoading = true;
        },
        loginUserSuccess(state, {payload}) {
            state.loginLoading = false;
            state.info = payload;
            state.loading = false;
            state.loginError = null;
        },
        loginUserFailure(state, action) {
            state.loginError = action.payload;
            state.loading = false;
            state.loginLoading = false;
        },
        logoutUserRequest(state, action) {

        },
        logoutUser(state) {
            state.info = null;
        },
        clearErrorUser(state) {
            state.registerError = null;
            state.loginError = null
        },
    }
});

export default usersSlice;