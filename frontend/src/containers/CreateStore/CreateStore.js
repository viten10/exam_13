import React, {useState} from 'react';
import {Checkbox, Grid, Typography} from "@mui/material";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FileInput from "../../components/UI/FileInput/FileInput";
import {useDispatch, useSelector} from "react-redux";
import {createInstitutionRequest} from "../../store/actions/institutionActions";

const CreateStore = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.info);
    const [institution, setInstitution] = useState({
        title: '',
        description: '',
        mainImage: null,
        user: '',
        permit: false,
    });

    const onchangeHandler = e => {
        const {name, value} = e.target;
        setInstitution(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const fileChangeHandler = e => {
        const files = e.target.files;

        setInstitution(prevState => {
            return {...prevState, mainImage: files[0]};
        });
    };

    const onSubmitForm = e => {
        e.preventDefault();
        institution.user = user._id;
        const formData = new FormData();
        Object.keys(institution).forEach(key => {
            formData.append(key, institution[key]);
        });

        dispatch(createInstitutionRequest(formData));
    };

    return (
        <Grid
            container
            justifyContent='column'
            textAlign='center'
        >
            <Grid item sx={{width: '50%'}}>
                <Typography gutterBottom variant='h6' component="div">Добавление Нового Объекта</Typography>
            </Grid>
            <Grid
                item
                sx={{width: '50%'}}
            >
                <Grid
                    component='form'
                    onSubmit={onSubmitForm}
                >
                    <Grid item sx={{width: '100%'}} my={2}>
                        <FormElement
                            label='Название заведения'
                            name='title'
                            fullWidth
                            required
                            value={institution.title}
                            onChange={onchangeHandler}
                        />
                    </Grid>
                    <Grid item my={2}>
                        <FormElement
                            label='Описание'
                            name='description'
                            fullWidth
                            required
                            value={institution.description}
                            onChange={onchangeHandler}
                        />

                    </Grid>
                    <Grid item sx={{width: '100%'}}>
                        <FileInput
                            name="mainImage"
                            type="file"
                            fullWidth
                            required={true}
                            onChange={fileChangeHandler}
                            // error={getFieldError('payment')}
                        />
                    </Grid>
                    <Grid item xs={12} sm={7} md={7} lg={5}>
                        <Grid>
                            <Checkbox
                                value={institution.permit}
                                name='permit'
                                onChange={() => setInstitution(prevState => ({
                                    ...prevState,
                                    permit: !prevState.permit,
                                }))}
                                inputProps={{'aria-label': 'controlled'}}
                            />
                            Я принимаю
                        </Grid>
                        <Grid sx={{width: '100%'}}>
                            <Typography>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A
                                culpa earum error eum excepturi expedita illum impedit labore
                                laudantium maiores minus nemo nulla praesentium, quis tempora ullam
                                vel. Dolores, ullam.
                            </Typography>

                        </Grid>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            sx={{marginBottom: '30px'}}
                            disabled={!institution.permit}
                        >
                            Принять
                        </ButtonWithProgress>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default CreateStore;