import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import History from "../../History";
import {toast} from "react-toastify";
import {
    fetchGalleryFailure,
    fetchGalleryRequest,
    fetchGallerySuccess,
    sendImageInstitutionByUserFailure,
    sendImageInstitutionByUserRequest,
    sendImageInstitutionByUserSuccess
} from "../actions/galleryActions";

export function* requestDataGallery({payload}) {
    try {
        const response = yield axiosApi.get(`/gallery/${payload.id}`);
        yield put(fetchGallerySuccess(response.data));
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(fetchGalleryFailure(e.response.data));
    }
}

export function* sendImageSagas({payload}) {
    try {
        const response = yield axiosApi.post(`/gallery`, payload);
        yield put(sendImageInstitutionByUserSuccess(response.data));
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(sendImageInstitutionByUserFailure(e.response.data));
    }
}

const galleriesSagas = [
    takeEvery(fetchGalleryRequest, requestDataGallery),
    takeEvery(sendImageInstitutionByUserRequest, sendImageSagas),

];

export default galleriesSagas;