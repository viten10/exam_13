import React from 'react';
import {Outlet} from "react-router-dom";
import {useSelector} from "react-redux";

const useAuth = () => {
    const user = useSelector(state => state.user.info);

    return user?.role === 'user';
};

const ProtectedRoutesForUser = () => {
    const isAuth = useAuth();

    return (
        isAuth ? <Outlet/> : <main style={{padding: "1rem"}}> <h1>Page Not Found</h1> </main>
    );
};

export default ProtectedRoutesForUser;