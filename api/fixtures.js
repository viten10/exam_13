const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const User = require("./models/User");
const Institution = require("./models/Institution");
const Gallery = require("./models/Gallery");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user, user2, admin] = await User.create(
        {
            email: "user@gmail.com",
            password: "12345678",
            token: nanoid(),
            role: "user",
            name: "User",
            avatar: 'fixtures/avatar1.jpeg',
        },
        {
            email: "user2@gmail.com",
            password: "12345678",
            token: nanoid(),
            role: "user",
            name: "User user",
            avatar: 'fixtures/avatar1.jpeg',
        },
        {
            email: "admin@gmail.com",
            password: "12345678",
            token: nanoid(),
            role: "admin",
            name: "Admin",
            avatar: 'fixtures/avatar2.jpeg',
        },
    );

    const [gallery1, gallery2, gallery3, gallery4, gallery5, gallery6, gallery7, gallery8] = await Gallery.create({
            user: user,
            image: 'fixtures/cafe-11.jpg',
        }, {
            user: user,
            image: 'fixtures/cafe-12.jpg',
        }, {
            user: user2,
            image: 'fixtures/cafe-21.jpg',
        }, {
            user: user2,
            image: 'fixtures/cafe-22.jpg',
        }, {
            user: user,
            image: 'fixtures/cafe-31.jpg',
        }, {
            user: user,
            image: 'fixtures/cafe-32.jpg',
        }, {
            user: user,
            image: 'fixtures/cafe-41.jpg',
        }, {
            user: user,
            image: 'fixtures/cafe-42.jpg',
        },
    );

    const [cafe1, cafe2, cafe3, cafe4] = await Institution.create(
        {
            title: 'Cafe - 1',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita incidunt magnam nihil, numquam recusandae unde ut! Animi dolorum laboriosam nostrum officiis perspiciatis possimus provident quae quasi, repudiandae voluptatum. Accusamus, accusantium beatae itaque laboriosam laborum quidem rerum sapiente veniam. Aspernatur atque cupiditate minus voluptatem! Asperiores cum distinctio ea error id illo ipsam libero non, omnis quae quia quis rem repellendus voluptatem.\n',
            user: user,
            mainImage: 'fixtures/cafe-1.jpeg',
            totalRating: {
                kitchen: 5,
                service: 5,
                atmosphere: 5,
            },
            gallery: [gallery1, gallery5, gallery6],
            reviewCount: 10,
        }, {
            title: 'Cafe - 2',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita incidunt magnam nihil, numquam recusandae unde ut! Animi dolorum laboriosam nostrum officiis perspiciatis possimus provident quae quasi, repudiandae voluptatum. Accusamus, accusantium beatae itaque laboriosam laborum quidem rerum sapiente veniam. Aspernatur atque cupiditate minus voluptatem! Asperiores cum distinctio ea error id illo ipsam libero non, omnis quae quia quis rem repellendus voluptatem.\n',
            user: user,
            mainImage: 'fixtures/cafe-2.jpeg',
            totalRating: {
                kitchen: 5,
                service: 4,
                atmosphere: 4.5,
            },
            gallery: [gallery2, gallery7],
            reviewCount: 10,

        }, {
            title: 'Cafe - 3',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita incidunt magnam nihil, numquam recusandae unde ut! Animi dolorum laboriosam nostrum officiis perspiciatis possimus provident quae quasi, repudiandae voluptatum. Accusamus, accusantium beatae itaque laboriosam laborum quidem rerum sapiente veniam. Aspernatur atque cupiditate minus voluptatem! Asperiores cum distinctio ea error id illo ipsam libero non, omnis quae quia quis rem repellendus voluptatem.\n',
            user: user,
            mainImage: 'fixtures/cafe-3.png',
            totalRating: {
                kitchen: 4,
                service: 3,
                atmosphere: 5,
            },
            gallery: [gallery3, gallery8],
            reviewCount: 10,

        }, {
            title: 'Cafe - 4',
            description: 'Cool 4',
            user: user,
            mainImage: 'fixtures/cafe-4.jpg',
            totalRating: {
                kitchen: 4,
                service: 4,
                atmosphere: 5,
            },
            gallery: gallery4,
            reviewCount: 10,
        },
    );

    await Gallery.findByIdAndUpdate(gallery1, {institution: cafe1});
    await Gallery.findByIdAndUpdate(gallery2, {institution: cafe2});
    await Gallery.findByIdAndUpdate(gallery3, {institution: cafe3});
    await Gallery.findByIdAndUpdate(gallery4, {institution: cafe4});
    await Gallery.findByIdAndUpdate(gallery5, {institution: cafe1});
    await Gallery.findByIdAndUpdate(gallery6, {institution: cafe1});
    await Gallery.findByIdAndUpdate(gallery7, {institution: cafe2});
    await Gallery.findByIdAndUpdate(gallery8, {institution: cafe1});

    await mongoose.connection.close();
};

run().catch(console.error);

