import InstitutionsSlice from "../slices/institutionSlice";

export const {
    fetchInstitutionRequest,
    fetchInstitutionSuccess,
    fetchInstitutionFailure,
    createInstitutionRequest,
    createInstitutionSuccess,
    createInstitutionFailure,
    increaseCountRequest,
} = InstitutionsSlice.actions;