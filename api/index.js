const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./routes/users');
const institution = require('./routes/institutions');
const gallery = require('./routes/galleries');
const review = require('./routes/reviews');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/users', users);
app.use('/gallery', gallery);
app.use('/institution', institution);
app.use('/review', review);


const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(config.port, () => {
        console.log('Port start on port: ', config.port);
    });

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.log(e));