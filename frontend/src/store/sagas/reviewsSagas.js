import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {mainPage} from '../../paths';
import History from "../../History";
import {
    fetchReviewFailure,
    fetchReviewRequest,
    fetchReviewSuccess,
    sendReviewInstitutionByUserFailure,
    sendReviewInstitutionByUserRequest,
    sendReviewInstitutionByUserSuccess
} from "../actions/reviewsActions";


export function* sendReviewSagas({payload}) {
    try {
        const response = yield axiosApi.post(`/review`, payload);
        yield put(sendReviewInstitutionByUserSuccess());
        History.push(mainPage);
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(sendReviewInstitutionByUserFailure(e.response.data));
    }
}

export function* fetchReviewSagas({payload}) {
    try {
        const response = yield axiosApi.get(`/review/?user=${payload.user}&institutions=${payload.institutions}`);
        yield put(fetchReviewSuccess(response.data));
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(fetchReviewFailure(e.response.data));
    }
}

const reviewsSagas = [
    takeEvery(sendReviewInstitutionByUserRequest, sendReviewSagas),
    takeEvery(fetchReviewRequest, fetchReviewSagas),
];

export default reviewsSagas;