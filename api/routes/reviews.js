const express = require("express");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Review = require("../models/Review");
const Institution = require("../models/Institution");


const router = express.Router();

router.get('/', async (req, res) => {
    try {

    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', auth, permit('user', 'admin'), async (req, res) => {
    const data = {};
    const newRating = {};
    try {
        if (req.body.institution) data.institution = req.body.institution;
        if (req.body.atmosphere) data.atmosphere = Number(req.body.atmosphere);
        if (req.body.kitchen) data.kitchen = Number(req.body.kitchen);
        if (req.body.service) data.service = Number(req.body.service);
        if (req.body.user) data.user = req.body.user;
        if (req.body.description) data.description = req.body.description;

        const response = await Review.findOne(
            {
                $or: [{user: req.body.user},{institution: req.body.institution}]
            });
        if (response) {
           return  res.status(409).send({error: "Вы уже оставляли отзыв"});
        }

        const review = new Review(data);
        await review.save();

        const institution = await Institution.findById(req.body.institution);
        newRating.kitchen = ((institution.totalRating.kitchen + req.body.kitchen) / 2).toFixed(1);
        newRating.service = ((institution.totalRating.service + req.body.service) / 2).toFixed(1);
        newRating.atmosphere = ((institution.totalRating.atmosphere + req.body.atmosphere) / 2).toFixed(1);

        console.log(typeof (newRating.kitchen));
        console.log(typeof (newRating.service));
        console.log(typeof (newRating.atmosphere));
        await Institution.findByIdAndUpdate(req.body.institution,
            {totalRating:
                    {
                        kitchen: Number(newRating.kitchen),
                        service: Number(newRating.service),
                        atmosphere: Number(newRating.atmosphere),
                    }
            });

        res.send(review);
    } catch (e) {
        res.status(500).send(e);
        console.log(e);
    }
});

router.put('/', auth, async (req, res) => {
    console.log('in put Review');

    try {

    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/', auth, async (req, res) => {
    console.log('in delete Review');

    try {

    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;