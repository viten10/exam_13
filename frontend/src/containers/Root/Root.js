import React, {useEffect} from 'react';
import {Container, Grid} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import MediaCard from "../../components/MediaCards/CardsComponent";
import {fetchInstitutionRequest} from "../../store/actions/institutionActions";

const Root = () => {
    const dispatch = useDispatch();
    const institutionsData = useSelector(state => state.institutions.data);

    useEffect(() => {
        dispatch(fetchInstitutionRequest());
    }, [dispatch]);
    console.log(institutionsData);
    return (
        <Container>
            <Grid container
                  justifyContent='space-between'

            >
                {institutionsData.map((card,i) =>(
                        <Grid
                            item
                            key={card._id}
                        >
                            <MediaCard
                                data = {card}
                                index = {i}
                            />
                        </Grid>
                    ))}
            </Grid>
        </Container>
    );
};

export default Root;