import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import History from '../../History';
import {mainPage} from "../../paths";
import {
    loginUserFailure,
    loginUserRequest,
    loginUserSuccess,
    logoutUser,
    logoutUserRequest,
    registerUserFailure,
    registerUserRequest,
    registerUserSuccess,
} from "../actions/usersActions";

export function* registerUserSaga({payload: data}) {
    console.log(data);
    try {
        const response = yield axiosApi.post('/users', data);
        yield put(registerUserSuccess(response.data));
        History.push(mainPage);
        toast.success('Register successfully');
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(registerUserFailure(e.response.data));
    }
}

export function* loginUserSaga({payload: data}) {
    try {
        const response = yield axiosApi.post('/users/sessions', data);
        yield put(loginUserSuccess(response.data));
        toast.success('Login successfully');
        History.push(mainPage);
    } catch (e) {
        toast.error(e.response.data.global);

        yield put(loginUserFailure(e.response.data));
    }
}

export function* loginOut({payload: data}) {
    try {
        yield axiosApi.delete('/users/sessions', {
            headers: {
                'Authorization': data.token,
            },
        })
        yield put(logoutUser());
        History.push(mainPage);
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(loginUserFailure(e.response.data));
    }
}

const userSaga = [
    takeEvery(registerUserRequest, registerUserSaga),
    takeEvery(loginUserRequest, loginUserSaga),
    takeEvery(logoutUserRequest, loginOut),
];

export default userSaga;