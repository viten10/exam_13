import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link, Link as RouterLink} from 'react-router-dom';
import {Alert, TextField} from "@mui/material";
import LockIcon from '@mui/icons-material/Lock';
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {clearErrorUser, registerUserRequest} from "../../store/actions/usersActions";
import {makeStyles} from "@mui/styles";
import {userLogin} from "../../paths";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%",
    },
}));

const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.user.registerError);
    const loading = useSelector(state => state.user.registerLoading);

    const [user, setUser] = useState({
        email: '',
        password: '',
        name: '',
        avatar: null,
    });

    useEffect(() => {
      return () => {
        dispatch(clearErrorUser());
      };
    }, [dispatch]);

    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(user).forEach(key => {
            formData.append(key, user[key]);
        });

        dispatch(registerUserRequest(formData));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].error;
        } catch (e) {
            return undefined;
        }
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setUser(prevState => {
            return {...prevState, [name]: file};
        });
    };

    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockIcon/>
                </Avatar>
                <Typography component="h1" variant="h6">
                    Sign up
                </Typography>

                {
                    error &&
                    <Alert severity="error" className={classes.alert}>
                        {error.error || error.global}
                    </Alert>
                }

                <Grid
                    component="form"
                    container
                    className={classes.form}
                    onSubmit={submitFormHandler}
                    spacing={2}
                >
                    <Grid item xs={12}>
                        <FormElement
                            required
                            autoComplete="off"
                            label="Name"
                            name="name"
                            value={user.name}
                            onChange={inputChangeHandler}
                            error={getFieldError('displayName')}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <FormElement
                            type="email"
                            required
                            autoComplete="new-email"
                            label="Email"
                            name="email"
                            value={user.email}
                            onChange={inputChangeHandler}
                            error={getFieldError('email')}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <FormElement
                            required
                            type="password"
                            label="Password"
                            name="password"
                            autoComplete="new-password"
                            value={user.password}
                            onChange={inputChangeHandler}
                            error={getFieldError('password')}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField
                            type="file"
                            name="avatar"
                            onChange={fileChangeHandler}
                            error={getFieldError('image')}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            loading={loading}
                            disabled={loading}
                        >
                            Sign Up
                        </ButtonWithProgress>
                    </Grid>

                    <Grid item container justifyContent="flex-end">
                        <Link component={RouterLink} variant="body2" to={userLogin}>
                            Already have an account? Sign in
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Register;