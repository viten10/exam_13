import reviewsSlice from "../slices/reviewsSlices";

export const {
    sendReviewInstitutionByUserRequest,
    sendReviewInstitutionByUserSuccess,
    sendReviewInstitutionByUserFailure,
    fetchReviewRequest,
    fetchReviewSuccess,
    fetchReviewFailure,
} = reviewsSlice.actions;