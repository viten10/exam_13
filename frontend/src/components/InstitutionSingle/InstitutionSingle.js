import React, {useEffect, useState} from 'react';
import {Container, Grid, Rating, Typography} from "@mui/material";
import {useLocation} from "react-router-dom";
import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {fetchGalleryRequest, sendImageInstitutionByUserRequest} from "../../store/actions/galleryActions";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import FileInput from "../UI/FileInput/FileInput";
import {fetchReviewRequest, sendReviewInstitutionByUserRequest} from "../../store/actions/reviewsActions";
import {increaseCountRequest} from "../../store/actions/institutionActions";

const InstitutionSingle = () => {
        const state = useLocation();
        const index = state.state.props.index;
        const data = state.state.props.data;
        const dispatch = useDispatch();
        const user = useSelector(state => state.user.info);
        const gallery = useSelector(state => state.galleries.data);
        const institutionsData = useSelector(state => state.institutions.data);
        let total = 0;
        if (data.totalRating) {
            console.log('in');
            total = (data.totalRating.kitchen + data.totalRating.service + data.totalRating.atmosphere) / 3;
        }

        const margin = 0.1;
        const [institution, setInstitution] = useState({
            title: institutionsData[index].title,
            description: institutionsData[index].description,
            mainImage: institutionsData[index].mainImage,
            totalRating: institutionsData[index].totalRating,
            reviewCount: institutionsData[index].reviewCount,
            review: institutionsData[index].review,
        });

        const [review, setReview] = useState({
            description: '',
            kitchen: 0,
            service: 0,
            atmosphere: 0,
            image: null,
        });

        useEffect(() => {
            dispatch(fetchGalleryRequest({id: data._id}));
            dispatch(fetchReviewRequest({user: user._id, institutions: data._id}));
            dispatch(increaseCountRequest({id: data._id}));
        }, [dispatch]);

        useEffect(() => {
            setInstitution(prevState => ({
                ...prevState,
                totalRating: institutionsData[index].totalRating,
                reviewCount: institutionsData[index].reviewCount,
                review: institutionsData[index].review,
            }))
        }, [institutionsData]);


        const onchangeDescriptionHandler = e => {
            const {name, value} = e.target;
            setReview(prevState => ({
                ...prevState,
                [name]: value
            }))
        };

        const onchangeRatingHandler = e => {
            const {name, value} = e.target;
            setReview(prevState => ({
                ...prevState,
                [name]: Number(value)
            }))
        };

        const fileChangeHandler = e => {
            const files = e.target.files;

            setReview(prevState => {
                return {...prevState, image: files[0]};
            });
        };


        const onSubmitForm = e => {
            e.preventDefault();
            dispatch(sendReviewInstitutionByUserRequest(
                {
                    description: review.description,
                    service: review.service,
                    kitchen: review.kitchen,
                    atmosphere: review.atmosphere,
                    institution: data._id,
                    user: user._id,
                }));
        };

        const onSubmitSendFileForm = e => {
            e.preventDefault();
            const dataS = {
                image: review.image,
                user: user._id,
                institution: data._id,
            };

            const formData = new FormData();
            Object.keys(dataS).forEach(key => {
                formData.append(key, dataS[key]);
            });

            dispatch(sendImageInstitutionByUserRequest(formData));
        };

        return (
            <Container>
                <Grid
                    container
                    alignItems='center'
                    justifyContent='space-between'

                >
                    <Grid
                        container
                        item
                        flexDirection='column'
                        sx={{width: '45%'}}
                    >
                        <Grid item>
                            <Typography gutterBottom variant="h6" component="div">
                                {data.title}
                            </Typography>
                        </Grid>
                        <Grid item>
                            {data.description}
                        </Grid>
                    </Grid>
                    <Grid item>
                        <img src={`${apiURL}/${data.mainImage}`} alt={data.title} width={400}/>
                    </Grid>
                </Grid>
                <Grid
                    container
                >
                    {gallery.map(img => (
                        <Grid
                            item
                            key={img._id}
                        >
                            <img src={`${apiURL}/${img.image}`} width={200}/>
                        </Grid>
                    ))}
                </Grid>
                <Grid
                    container
                    flexDirection='row'
                >
                    <Grid
                        container
                        item
                        flexDirection='column'
                        sx={{width: '150px'}}
                    >
                        <Grid item>
                            <Typography gutterBottom variant='h6' component="div">Рейтинг</Typography>
                        </Grid>
                        <Grid item>
                            <Typography gutterBottom variant="body1" component="div">Общий рейтинг</Typography>
                        </Grid>
                        <Grid item>
                            <Typography gutterBottom variant="body1" component="div">Сервис </Typography>
                        </Grid>
                        <Grid item>
                            <Typography gutterBottom variant="body1" component="div">Кухня </Typography>
                        </Grid>
                        <Grid item>
                            <Typography gutterBottom variant="body1" component="div">Атмосфера </Typography>
                        </Grid>
                    </Grid>

                    <Grid
                        container
                        item
                        flexDirection='column'
                        sx={{width: '40%'}}
                        mt={4}

                    >
                        <Grid item my={margin}>
                            <Rating
                                readOnly
                                name="rating"
                                precision={0.1}
                                value={total}
                            />
                        </Grid>
                        <Grid item my={margin}>
                            <Rating
                                readOnly
                                name="rating"
                                precision={0.1}
                                value={data.totalRating ? data.totalRating.service : 0}
                            />
                        </Grid>
                        <Grid item my={margin}>
                            <Rating
                                readOnly
                                name="rating"
                                precision={margin}
                                value={data.totalRating ?data.totalRating.kitchen : 0}
                            />
                        </Grid>
                        <Grid item my={margin}>
                            <Rating
                                readOnly
                                name="rating"
                                precision={0.1}
                                value={data.totalRating ? data.totalRating.atmosphere : 0}
                            />
                        </Grid>
                    </Grid>

                    <Grid
                        container
                    >
                        <Grid
                            container
                            component='form'
                            onSubmit={onSubmitForm}
                        >
                            <FormElement
                                label='Добавить комментарий'
                                name='description'
                                fullWidth
                                value={review.description}
                                onChange={onchangeDescriptionHandler}
                            />
                            <Grid
                                container
                                flexDirection='column'

                            >
                                <Grid item>
                                    <Typography component="legend">Сервис</Typography>
                                    <Rating
                                        precision={0.1}
                                        name="service"
                                        value={review.service}
                                        onChange={onchangeRatingHandler}
                                    />
                                </Grid>
                                <Grid item>
                                    <Typography component="legend">Кухня</Typography>
                                    <Rating
                                        name="kitchen"
                                        value={review.kitchen}
                                        precision={0.1}
                                        onChange={onchangeRatingHandler}
                                    />
                                </Grid>
                                <Grid item>
                                    <Typography component="legend">Атмосфера</Typography>
                                    <Rating
                                        name="atmosphere"
                                        value={review.atmosphere}
                                        precision={0.1}
                                        onChange={onchangeRatingHandler}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12} sm={7} md={7} lg={5}>
                                <ButtonWithProgress
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    sx={{marginBottom: '30px'}}
                                >
                                    Принять
                                </ButtonWithProgress>
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            component='form'
                            flexDirection='column'
                            onSubmit={onSubmitSendFileForm}

                        >
                            <Grid item sx={{width: '100%'}}>
                                <Typography> Загруить фото</Typography>
                            </Grid>
                            <Grid item sx={{width: '50%'}}>
                                <FileInput
                                    name="image"
                                    type="file"
                                    fullWidth
                                    required={true}
                                    onChange={fileChangeHandler}
                                    // error={getFieldError('payment')}
                                />
                            </Grid>
                            <Grid item sx={{width: '30%'}} my={2}>
                                <ButtonWithProgress
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    sx={{marginBottom: '30px'}}
                                >
                                    Принять
                                </ButtonWithProgress>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>

        );
    }
;


export default InstitutionSingle;